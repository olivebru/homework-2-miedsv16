#include <grpcpp/grpcpp.h>
#include <string>
#include "sum.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

using sum::sumRequest;
using sum::sumResponse;
using sum::Sum;

class sumClient {
 public:
  sumClient(std::shared_ptr<Channel> channel)
      : stub_(Sum::NewStub(channel)) {}

  // Assembles client payload, sends it to the server, and returns its response
  int sendRequest(int a, int b, int c) {
    // Data to be sent to server
    sumRequest request;
    
    /* settings of parameters for the request */   
    request.set_a(a);
    request.set_b(b);
    request.set_c(c); 


    // Container for server response
    sumResponse reply;

    // Context can be used to send meta data to server or modify RPC behaviour
    ClientContext context;

    // Actual Remote Procedure Call
    Status status = stub_->sumNumbers(&context, request, &reply);

    // Returns results based on RPC status
    if (status.ok()) {
      return reply.sum();
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return -1;
    }
  }

 private:
  std::unique_ptr<Sum::Stub> stub_;
};

void RunClient() {
  std::string target_address("0.0.0.0:50052");
  // Instantiates the client
  sumClient client(
      // Channel from which RPCs are made - endpoint is the target_address
      grpc::CreateChannel(target_address,
                          // Indicate when channel is not authenticated
                          grpc::InsecureChannelCredentials()));

 
  // RPC is created and response is stored
  int a=1,b=2,c=3;
  int response = client.sendRequest(a,b,c);

  // Prints results
  std::cout << "sum: " << response << std::endl;
}

int main(int argc, char* argv[]) {
  RunClient();

  return 0;
}